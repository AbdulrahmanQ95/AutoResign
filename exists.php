<?php
if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    exit('error');
}

$file = isset($_POST['file']) ? trim($_POST['file']) : '';

if($file == '') {
    exit('error');
}

if(!file_exists(getcwd() . '/completed/' . $file . '.ipa')) {
    exit('error');
}

echo 'success';
?>
