<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require 'functions.php';
require 'vendor/autoload.php';

if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
    exit(json_encode('error', 422));
}

$privateKey          = isset($_POST['private_key'])           ? trim($_POST['private_key'])             : '';
$ipaFile             = isset($_POST['ipa_file'])              ? trim($_POST['ipa_file'])                : '';
$mobileprovisionFile = isset($_POST['mobileprovision_file'])  ? trim($_POST['mobileprovision_file'])    : '';
$ipaName             = isset($_POST['ipa_name'])              ? trim($_POST['ipa_name'])                : '';
$appName             = isset($_POST['app_name'])              ? trim($_POST['app_name'])                : '';
$duplicateNum        = isset($_POST['duplicate_num'])         ? intval($_POST['duplicate_num'])         : 0;
$developerTeamKey    = isset($_POST['developer_team_key'])    ? trim($_POST['developer_team_key'])      : '';



if($privateKey == '' || $privateKey != 'On7aY1Lb7G-fYOqWi7B6N-HG6tFGgVTw-2vF7Wc3jKf' || $ipaFile == '' || $mobileprovisionFile == '' || $ipaName == '' || $developerTeamKey == '') {
    exit(json_encode('some var are null', 422));
}


// general vars
$ipaDir                    = getcwd() . '/wait/';
$completedDir              = getcwd() . '/completed/';
$randIPAName               = $ipaName .'.ipa';
$randMobileprovisionFile   = md5(time().rand(10000000,90000000)) .'.mobileprovision';
$ipaPath                   = $ipaDir . '/' . $randIPAName;
$mobileprovisionPath       = $ipaDir . '/' . $randMobileprovisionFile;


if(file_exists($ipaPath)) {
    exit(json_encode('we cannot cretae ' . $ipaFile .' file', 422));
}

// clean completed ipa ..
$completedIPAs = scandir($completedDir);
foreach($completedIPAs as $completedIpaKey => $completedIpaFile) {
    if(in_array($completedIpaFile, ['.', '..']) || is_dir($completedDir .'/'. $completedIpaFile)) {
        continue;
    }

    if( time() > (filemtime($completedDir .'/'. $completedIpaFile) + 1800) ) {
        unlink($completedDir .'/'. $completedIpaFile);
    }
}


// start download ipa file.
$tempIpaFile = fopen($ipaPath, 'w');

try {
    $client      = new \GuzzleHttp\Client();
    $response    = $client->request('GET', $ipaFile, ['save_to' => $tempIpaFile]);
}
catch (Exception $e) {
    deleteDir($ipaPath);
    exit(json_encode('we cannot download the application 1', 422));
}

if($response->getStatusCode() != 200) {
    deleteDir($ipaPath);
    exit(json_encode('we cannot download the application 2', 422));
}


// verify the ipa file
$ipaInfo = pathinfo($ipaPath);
if($ipaInfo['extension'] != 'ipa') {
    deleteDir($ipaPath);
    exit(json_encode('we cannot download the application 3', 422));
}

// start download mobileprovision file
try {
    $tempMobileprovisionFile = fopen($mobileprovisionPath, 'w');
    $response                = $client->request('GET', $mobileprovisionFile, ['save_to' => $tempMobileprovisionFile]);
}
catch (Exception $e) {
    unlink($ipaPath);
    unlink($mobileprovisionPath);
    exit(json_encode('we cannot download the application 4', 422));
}

// run setup tool
exec('sh ' . getcwd() .'/shellScript/setup.sh "'. $mobileprovisionPath .'" "'. $ipaPath .'"');

// check if the request need to duplicate app
if($duplicateNum > 0) {
    exec('sh ' . getcwd() .'/shellScript/duplicate.sh "'. $duplicateNum .'" "'. $ipaPath .'"');
}

// heck if the request need to chnage app name
if($appName != '') {
    exec('sh ' . getcwd() .'/shellScript/changeName.sh "'. $appName .'" "'. $ipaPath .'"');
}

// run resign tool
$BUNDLEID = exec('sh ' . getcwd() .'/shellScript/resign.sh "iPhone Distribution: '. $developerTeamKey .'" "'. $ipaPath .'"');

// get app name
$appName  = exec('sh ' . getcwd() .'/shellScript/getAppName.sh "'. $ipaPath .'"');

// run zipping & remove remnants tool
exec('sh ' . getcwd() .'/shellScript/zipping.sh "'. $ipaPath .'" "'. $completedDir .'"');

unlink($ipaPath);
unlink($mobileprovisionPath);

exit(json_encode(['ipa' => $randIPAName, 'bundle_id' => $BUNDLEID, 'app_name' => $appName], 200));
