#!/bin/bash
appName="$1"
IPAFILE="$2"

filename=$(basename "$IPAFILE" .ipa)
tempextracted="/tmp/N_extracted"$filename
APPLICATION=$(ls "$tempextracted"/Payload/)

/usr/libexec/PlistBuddy -c "Set :CFBundleDisplayName $appName" "$tempextracted/Payload/$APPLICATION/Info.plist"
/usr/libexec/PlistBuddy -c "Set :CFBundleIdentifier $appName" "$tempextracted/Payload/$APPLICATION/Info.plist"
