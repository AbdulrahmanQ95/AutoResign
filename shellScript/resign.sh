#!/bin/bash
DEVID="$1"
IPAFILE="$2"

filename=$(basename "$IPAFILE" .ipa)
tempextracted="/tmp/N_extracted"$filename
APPLICATION=$(ls "$tempextracted"/Payload/)


rm -rf "$tempextracted/Payload/$APPLICATION/PlugIns"
rm -rf "$tempextracted/Payload/$APPLICATION/Watch"


find "$tempextracted/Payload/$APPLICATION/libloader" -type f > /tmp/N_directories"$filename".txt 2>/dev/null
find -d $tempextracted \( -name "*.app" -o -name "*.appex" -o -name "*.framework" -o -name "*.dylib"  \) >> /tmp/N_directories"$filename".txt
security cms -D -i $tempextracted"/Payload/$APPLICATION/embedded.mobileprovision" >> /tmp/N_entitlements_full"$filename".plist 2>/dev/null
/usr/libexec/PlistBuddy -x -c 'Print:Entitlements' /tmp/N_entitlements_full"$filename".plist >> /tmp/N_entitlements"$filename".plist
while IFS='' read -r line || [[ -n "$line" ]]; do
    /usr/bin/codesign --continue -f -s "$DEVID" --entitlements "/tmp/N_entitlements"$filename".plist"  "$line" >/dev/null 2>&1
done < /tmp/N_directories"$filename".txt


BUNDLE_ID=$(/usr/libexec/PlistBuddy -c "Print CFBundleIdentifier"  "$tempextracted/Payload/$APPLICATION/Info.plist")
echo $BUNDLE_ID
