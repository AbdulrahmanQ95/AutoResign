#!/bin/bash
IPAFILE="$1"
IPAOUT="$2"

filename=$(basename "$IPAFILE" .ipa)
tempextracted="/tmp/N_extracted"$filename

cd $tempextracted
zip -qry $IPAOUT"/"$filename".ipa" *

rm -rf $tempextracted
rm /tmp/N_directories"$filename".txt
rm /tmp/N_entitlements"$filename".plist
rm /tmp/N_entitlements_full"$filename".plist
