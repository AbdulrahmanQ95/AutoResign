#!/bin/bash
IPAFILE="$1"

filename=$(basename "$IPAFILE" .ipa)
tempextracted="/tmp/N_extracted"$filename
APPLICATION=$(ls "$tempextracted"/Payload/)

appName=$(/usr/libexec/PlistBuddy -c "Print CFBundleDisplayName"  "$tempextracted/Payload/$APPLICATION/Info.plist")
echo $appName
